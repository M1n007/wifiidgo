const fetch = require('node-fetch');

const doGetListInbox = (uname) => new Promise((resolve, reject) => {
    fetch(`https://getnada.com/api/v1/inboxes/${uname}@getnada.com`, {
        method:'GET',
        headers: {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
            'referer': 'https://getnada.com/',
            'authority': 'getnada.com',
            'cookie': 'tarteaucitron=\u0021adsense=true\u0021gajs=true; __utmz=117362832.1569524309.1.1.utmcsr=app.getnada.com|utmccn=(referral)|utmcmd=referral|utmcct=/; __utmc=117362832; __utmt=1; __utma=117362832.1419388649.1569524231.1569524231.1569563555.2; __utmb=117362832.1.10.1569563556'
        }
    })
    .then(res => res.json())
    .then(res => {
        resolve(res)
    })
    .catch(err => {
        reject(err)
    })
});

const doGetLinkForVeryfication = (uid) => new Promise((resolve, reject) => {
    fetch(`https://getnada.com/api/v1/messages/${uid}`, {
        method:'GET',
        headers: {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
            'referer': 'https://getnada.com/',
            'authority': 'getnada.com',
            'cookie': 'tarteaucitron=\u0021adsense=true\u0021gajs=true; __utmz=117362832.1569524309.1.1.utmcsr=app.getnada.com|utmccn=(referral)|utmcmd=referral|utmcct=/; __utmc=117362832; __utmt=1; __utma=117362832.1419388649.1569524231.1569524231.1569563555.2; __utmb=117362832.1.10.1569563556'
        }
    })
    .then(res => res.json())
    .then(res => {
        resolve(res)
    })
    .catch(err => {
        reject(err)
    })
});

module.exports = {
    doGetListInbox,
    doGetLinkForVeryfication,
}