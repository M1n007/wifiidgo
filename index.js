const fetch = require('node-fetch');
const fs = require('async-file');
const moment = require('moment');
const readline = require('readline-sync');
const getNada = require('./lib/getnadaRequest');
const cheerio = require('cheerio');
const delay = require('delay');

const countAccount = readline.question("Berapa akun yang akan anda buat ? ");

function randNumber(length) {
	result = '';
	const characters = '0123456789';
	const charactersLength = characters.length;
	for (let i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

function randstr(length) {
	result = '';
	const characters = '012345678910abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	const charactersLength = characters.length;
	for (let i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}



const generateName = () => new Promise((resolve, reject) => {
    fetch('https://api.randomuser.me/', {
        method:'GET'
    })
    .then(res => res.json())
    .then(res => {
        resolve(res)
    })
    .catch(err => {
        reject(err)
    })
})

const doRegister = (name) => new Promise((resolve,reject) => {
    fetch('https://wigo.wifi.id/api/account/register_tmoney', {
        method:'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Host': 'wigo.wifi.id',
            'User-Agent': 'okhttp/3.10.0'
        },
        body: `userName=${name.first}@getnada.com&password=coegSekali1&accType=1&fullName=${name.first} ${name.last}&phoneNo=0822148${randNumber(5)}&terminal=MYWIFI&apiKey=T-MONEY_ZTRi${randstr(36)}`
    })
    .then(res => res.json())
    .then(res => {
        resolve(res)
    })
    .catch(err => {
        reject(err);
    })
});

const doLogin = (name) => new Promise((resolve,reject) => {
    fetch('https://wigo.wifi.id/api/account/auth_tmoney', {
        method:'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Host': 'wigo.wifi.id',
            'User-Agent': 'okhttp/3.10.0'
        },
        body: `userName=${name.first}@getnada.com&password=coegSekali1&datetime=2019-09-26 12:37:35&terminal=MYWIFI&apiKey=T-MONEY_ZTRiMWNiOWE4MjE1MDhlNDc0M2UxMzkyYzEyODEz&androidID=6cb9${randstr(4)}6a9ed176&deviceUID=2019_09_26_19_26_59__com_telkom_wifiidgo_69_5_7_0__com_telkom_wifiidlibrary_${randstr(4)}_3_11__5_0_2__BarnesAndNoble__NOOK__Indosat_Ooredoo__5d8cae93cc${randstr(4)}`
    })
    .then(res => res.json())
    .then(res => {
        resolve(res)
    })
    .catch(err => {
        reject(err);
    })
});

const doRedeem = (idTmoney) => new Promise((resolve,reject) => {
    fetch('https://wigo.wifi.id/api/redeem', {
        method:'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Host': 'wigo.wifi.id',
            'User-Agent': 'okhttp/3.10.0'
        },
        body: `idTmoney=${idTmoney}`
    })
    .then(res => res.json())
    .then(res => {
        resolve(res)
    })
    .catch(err => {
        reject(err);
    })
});

const doListInbox = (idTmoney, idFusion, token) => new Promise((resolve,reject) => {
    fetch('https://wigo.wifi.id/api/list_inbox', {
        method:'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Host': 'wigo.wifi.id',
            'User-Agent': 'okhttp/3.10.0'
        },
        body: `idTmoney=${idTmoney}&idFusion=%2B${idFusion}&token=${token}&terminal=MYWIFI&apiKey=T-MONEY_ZTRiMWNiOWE4MjE1MDhlNDc0M2UxMzkyYzEyODEz`
    })
    .then(res => res.json())
    .then(res => {
        resolve(res)
    })
    .catch(err => {
        reject(err);
    })
});

const doVeryf = (url) => new Promise((resolve, reject) => {
    fetch(`${url}`, {
        method:'GET',
        headers: {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
        }
    })
    .then(res => res.text())
    .then(res => {
        resolve(res)
    })
    .catch(err => {
        reject(err)
    })
});



(async () => {
    for (let index = 0; index < countAccount; index++) {
        const name = await generateName()
        const emel = `${name.results[0].name.first}@getnada.com`
        console.log('');
        console.log('');
        console.log(`[${moment().format("HH:mm:ss")}] Mencoba mendaftar dengan email : ${emel}`);
        const register = await doRegister(name.results[0].name);
        if (register.error !== false) {
            console.log(`[${moment().format("HH:mm:ss")}] Ada masalah saat mendaftar, silahkan coba lagi.`);
            console.log(`[${moment().format("HH:mm:ss")}] Message : ${register.message}`);
        }else{
            console.log(`[${moment().format("HH:mm:ss")}] Register sukses.`);
            console.log(`[${moment().format("HH:mm:ss")}] Mencoba Mendapatkan link.`);
            await delay(20000);
            const listInbox = await getNada.doGetListInbox(emel);
            while(true){
                for (let index = 0; index < listInbox.msgs.length; index++) {
                    const element = listInbox.msgs[index];
                    if (element.fe === 'go@wifi.id') {
                        console.log(`[${moment().format("HH:mm:ss")}] Mendapatkan uid untuk get link : ${element.uid}`);
                        const getLink = await getNada.doGetLinkForVeryfication(element.uid);
                        const $ = cheerio.load(getLink.html);
                        const link = $('a.btn.btn-success').attr('href')
                        if (link) {
                            console.log(`[${moment().format("HH:mm:ss")}] Berhasil mendapatkan link : ${link}`);
                            const veryf = await doVeryf(link);
                            const $ = cheerio.load(veryf);
                            const message = $('div.header h1').text();
                            console.log(`[${moment().format("HH:mm:ss")}] ${message}`);
                        }
                        break;
                    }                   
                }
                break;
            }
            console.log(`[${moment().format("HH:mm:ss")}] Mencoba Login.`);
            const login = await doLogin(name.results[0].name);
            if (login.error !== false) {
                console.log(`[${moment().format("HH:mm:ss")}] Ada masalah saat login.`);
                console.log(`[${moment().format("HH:mm:ss")}] ${JSON.stringify(login)}`);
            }else{
                if (login.data.loyalty === 0) {
                    console.log(`[${moment().format("HH:mm:ss")}] Oops, anda tidak memiliki loyalty silahkan coba lagi.`);
                    console.log(`[${moment().format("HH:mm:ss")}] Poin yang anda miliki : ${login.data.loyalty}`);
                }else{
                    console.log(`[${moment().format("HH:mm:ss")}] Poin yang anda miliki : ${login.data.loyalty}`);
                    console.log(`[${moment().format("HH:mm:ss")}] Mencoba untuk meRedeem point.`);
        
        
                    for (let index = 0; index < 2; index++) {
                        const redeem = await doRedeem(login.data.idTmoney);
                    }
        
                    console.log(`[${moment().format("HH:mm:ss")}] Mengecek voucher.`);
        
                    const idTmoney = login.data.idTmoney;
                    const idFusion = login.data.idFusion.replace('+', '')
                    const token = login.data.token;
                    const getListInbox = await doListInbox(idTmoney, idFusion, token);
        
                    if (getListInbox.data.length === 0) {
                        console.log(`[${moment().format("HH:mm:ss")}] No message found.`)
                    }
                    
                    await getListInbox.data.map(account => {
                        if (account.username !== null) {
                            console.log(`[${moment().format("HH:mm:ss")}] Berhasil mendapatkan akun wifi id ${account.username}:${account.password}`)
                            fs.appendFile('dataAkun.txt', `\n${account.username}:${account.password}\n`, 'utf8')
                        }
                    })
                }
            }
        }
        
    }
    
})();